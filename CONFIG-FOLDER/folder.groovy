/*##########################################################################
                                Main Folder
############################################################################*/

folder('DEVELOPMENT') {
    description('this folder contains all DEVELOPMENT related jobs')
}

/*##########################################################################
                                Project Folder
############################################################################*/

folder('DEVELOPMENT/POS-INDONESIA') {
    description('this folder BACKEND related jobs')
}

/*##########################################################################
                                Environment Folder
############################################################################*/

folder('DEVELOPMENT/POS-INDONESIA/BACKEND') {
    description('this folder BACKEND related jobs')
}

folder('DEVELOPMENT/POS-INDONESIA/FRONTEND') {
    description('this folder BACKEND related jobs')
}