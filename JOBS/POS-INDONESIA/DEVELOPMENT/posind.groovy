import groovy.json.JsonSlurper

def createJobsFromJson() {
    def jsonFileContent = readFileFromWorkspace('CONFIG-JSON/POS-INDONESIA/DEVELOPMENT/posind.json')
    def jsonSlurper = new JsonSlurper()
    def jsonData = jsonSlurper.parseText(jsonFileContent)

    jsonData.services[0].jobs.each { job ->
        def jobName = job.JOBNAME
        def environment = job.ENVIRONMENT
        def directory = job.DIRECTORY
        def project = job.PROJECT

        def randomString = UUID.randomUUID().toString().replaceAll('-', '')

        pipelineJob("${environment}/${project}/${directory}/${jobName}") {
            triggers {
                genericTrigger {
                    token("${randomString}")
                    tokenCredentialId('')
                    printContributedVariables(false)
                    printPostContent(false)
                    silentResponse(true)
                }
            }
            definition {
                cps {
                    script(readFileFromWorkspace('PIPELINE/POS-INDONESIA/DEVELOPMENT/jenkinsfile'))
                    sandbox()
                }
            }
        }
    }
}

createJobsFromJson()
